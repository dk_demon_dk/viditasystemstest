import django
from django.db import models


class PeopleName(models.Model):
    name = models.CharField(max_length=200,
                            null=True,
                            blank=True)

    photo = models.ForeignKey(to="PhotoManager",
                              related_name='peoplenames',
                              on_delete=models.RESTRICT)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "people_name"


class PhotoManager(models.Model):
    photo = models.ImageField(upload_to='image',
                              null=True,
                              blank=True)

    geo_location = models.CharField(max_length=200,
                                    null=True,
                                    blank=True)

    description = models.TextField(max_length=3000,
                                   null=True,
                                   blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.photo

    class Meta:
        db_table = "photo_manager"
