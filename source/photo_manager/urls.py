from django.urls import path

from .views import PhotoCreate, PhotoList, PhotoDetail, PeopleNameList

app_name = "photo_manager"

urlpatterns = [
    path("photo-create/", PhotoCreate.as_view(), name="photo_create"),
    path("photo-list/", PhotoList.as_view(), name="photo_list"),
    path("photo/<int:pk>/", PhotoDetail.as_view(), name="photo_detail"),
    path("name-list/", PeopleNameList.as_view(), name="name_list")
]
