from django.utils.decorators import method_decorator
from drf_yasg import openapi
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema

from .models import PhotoManager, PeopleName
from .serializers import PhotoManagerSerializer, PhotoManagerSerializerWithoutMeta, PeopleNameSerializer


@method_decorator(name='post', decorator=swagger_auto_schema(
    operation_description="description from swagger_auto_schema via method_decorator",
    tags=["Photo"],
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={"photo": {"type": "base64"}, "geo_location": {"type": "string"}, "description": {"type": "string"},
                    "peoplenames": {"type": "array", "items": {"type": "object",
                                                               "properties": {"name": {"type": "string"}}}}}
    )

))
class PhotoCreate(CreateAPIView):
    serializer_class = PhotoManagerSerializer
    permission_classes = (IsAuthenticated,)


@method_decorator(name='get', decorator=swagger_auto_schema(
    operation_description="description from swagger_auto_schema via method_decorator",
    tags=["Photo"]
))
class PhotoList(ListAPIView):
    queryset = PhotoManager.objects.all()
    serializer_class = PhotoManagerSerializerWithoutMeta

    def get_queryset(self):
        queryset = PhotoManager.objects.all()
        geo_location = self.request.query_params.get('geo_location')
        description = self.request.query_params.get('description')
        created_at = self.request.query_params.get('created_at')
        name = self.request.query_params.get('name')
        if geo_location is not None:
            queryset = queryset.filter(geo_location__icontains=geo_location)
        if description is not None:
            queryset = queryset.filter(description__icontains=description)
        if created_at is not None:
            queryset = queryset.filter(created_at__gte=created_at)
        if name is not None:
            queryset = queryset.filter(peoplenames__name__icontains=name)
        return queryset


@method_decorator(name='get', decorator=swagger_auto_schema(
    operation_description="description from swagger_auto_schema via method_decorator",
    tags=["Photo"]
))
class PhotoDetail(RetrieveAPIView):
    queryset = PhotoManager.objects.all()
    serializer_class = PhotoManagerSerializer


class PeopleNameList(ListAPIView):
    queryset = PeopleName.objects.all()
    serializer_class = PeopleNameSerializer

    def get_queryset(self):
        queryset = PeopleName.objects.all().values('name').distinct('name')
        name = self.request.query_params.get('name')
        if name is not None:
            queryset = queryset.filter(name__icontains=name).values('name').distinct('name')
        return queryset
