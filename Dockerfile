FROM python:latest

RUN mkdir /server && apt-get update && apt-get install -y git libpq-dev postgresql-client
WORKDIR /server

COPY poetry.lock pyproject.toml /server/

RUN pip install poetry && poetry config virtualenvs.create false && poetry install --no-root

COPY source /server